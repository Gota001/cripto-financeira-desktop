﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CriptoFinanceiraDesktop.Modelo;

namespace CriptoFinanceiraDesktop
{
    public partial class AdicionarUsuario : Form
    {
        public AdicionarUsuario()
        {
            InitializeComponent();
        }

        private void AdicionarUsuario_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
        }

        private void btnCriarUsuario_Click(object sender, EventArgs e)
        {
            bool campoVazio = false;
            String[] texto = {"","","","",""};
            String sexo = "";

            if (rbMasculino.Checked) 
            { 
                sexo = "M";
            }

            if (rbFeminino.Checked)
            {
                sexo = "F";
            }
            TextBox[] tb = {txbNome, txbMatricula, txbCargo, txbEmail, txbCpf};
            for (int i = 0; i < tb.Length; i++)
            {
                if (tb[i].Text == string.Empty)
                {
                    campoVazio = true;
                }
                else
                {
                    texto[i] = tb[i].Text;
                }
    
            }

            if (sexo.Equals(""))
            {
                campoVazio = true;
            }

            if (campoVazio)
            {
                MessageBox.Show("Preencha todos os campos com dados válidos", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else
            {
                String nome = texto[0];
                String matricula = texto[1];
                String cargo = texto[2];
                String email = texto[3];
                String cpf = texto[4];

                Controle controle = new Controle();
                controle.cadastrar(nome,matricula,cargo,email,cpf,sexo);

                if (controle.mensagem.Equals(""))
                {
                    if (controle.tem)
                    {
                        MessageBox.Show("Usuário cadastrado com sucesso!\nAnote os dados: \n\n" +
                            "Usuário: " + controle.usuario + "\n Senha: " + controle.senha, "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        txbNome.Text = ""; txbMatricula.Text = ""; txbCargo.Text = ""; txbEmail.Text = ""; txbCpf.Text = "";

                    }
                    else
                    {
                        MessageBox.Show("Erro ao cadastrar usuário, tente novamente", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show(controle.mensagem);
                }


            }

        }

    }
}
