﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CriptoFinanceiraDesktop.Modelo;
using CriptoFinanceiraDesktop.ModuloUsuarios;

namespace CriptoFinanceiraDesktop
{
    public partial class Login : Form
    {
        Principal principal = new Principal();
        public bool log = false;
        public Login()
        {
            InitializeComponent();

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            CarregarLogin carrega = new CarregarLogin();
            carrega.Show();
            
            Controle controle = new Controle();
            controle.acessar(mkdUsuario.Text, mkdSenha.Text);


            if (controle.mensagem.Equals("")) { 
                if (controle.tem) 
                {
                    carrega.Close();
                    MessageBox.Show("login Efetuado com sucesso!","Sucesso", MessageBoxButtons.OK , MessageBoxIcon.Information);
                    
                    
                    log = true;
                    Inicio.recuperarInstancia().instancePrincipal.log = this.log;
                    Inicio.recuperarInstancia().instancePrincipal.Enabled = true;
                    this.Close();

                }
                else
                {
                    carrega.Close();
                    MessageBox.Show("Usuário e senha não encontrado, verifique login e senha", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                carrega.Close();
                MessageBox.Show(controle.mensagem);
            }

        }

        private void Login_Load(object sender, EventArgs e)
        {
            
        }
    }
}
