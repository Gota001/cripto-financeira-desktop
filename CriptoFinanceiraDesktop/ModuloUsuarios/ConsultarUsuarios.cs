﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CriptoFinanceiraDesktop.Modelo;
using System.Data.SqlClient;
using CriptoFinanceiraDesktop.Backend;

namespace CriptoFinanceiraDesktop
{
    public partial class ConsultarUsuarios : Form
    {
        public ConsultarUsuarios()
        {
            InitializeComponent();
        }

        public String sexo;
        public String nome;
        public String email;
        public String cpf;
        public String cargo;

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Controle controle = new Controle();
            String[] campos = controle.consulta(txbMatricula.Text);

            txbNome.Text = campos[0];
            txbCargo.Text = campos[1];
            txbEmail.Text = campos[2];
            txbCpf.Text = campos[3];
            sexo = campos[4];

            if (sexo.Equals("F"))
            {
                rbFeminino.PerformClick();
            }
            if (sexo.Equals("M"))
            {
                rbMasculino.PerformClick();
            }
            if (sexo.Equals(""))
            {
                MessageBox.Show("Usuário não encontrado!", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }

        private void ConsultarUsuarios_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            Controle controle = new Controle();
            dgvUsuarios.DataSource = controle.consultarUsuario();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            bool update = false;
            bool campo = false;

            String[] texto = { nome, cargo, email, cpf };
            TextBox[] tb = { txbNome, txbCargo, txbEmail, txbCpf };
            String sexoAux = "";
            String[] alterar = { "nome", "cargo", "email", "cpf" };
            String matricula = txbMatricula.Text;
            Controle controle = new Controle();
          

            if (rbFeminino.Checked)
            {
                sexoAux = "F";
            }
            if (rbMasculino.Checked)
            {
                sexoAux = "M";

            }

            for (int i = 0; i < tb.Length; i++)
            {
                if (tb[i].Text == String.Empty)
                {
                    campo = true;
                } 

                if (sexoAux == String.Empty)
                {
                    campo = true;
                }

                if (matricula == String.Empty)
                {
                    campo = true;
                }


            }
            if (campo)
            {
                MessageBox.Show("Preencha todos os campos", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            if (!campo) { 
            String[] aux = controle.consulta(matricula);
            texto[0] = aux[0];  texto[1] = aux[1]; texto[2] = aux[2]; texto[3] = aux[3]; 
            for (int i = 0; i < texto.Length; i++)
            {
                if (!texto[i].Equals(tb[i].Text))
                {
                    update = controle.alterar(alterar[i], tb[i].Text, matricula);
                }
            }

            if (!sexo.Equals(sexoAux))
            {
                update = controle.alterar("sexo", sexoAux, matricula);
            }

            }

            if (update)
            {
                MessageBox.Show("Alteração efetuada com sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dgvUsuarios.DataSource = controle.consultarUsuario();
            }
            else if (!update)
            {
                MessageBox.Show("Sem alterações para efetuar", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                MessageBox.Show(controle.mensagem, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            update = false;
        }
    }
}
