﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CriptoFinanceiraDesktop.Modelo;

namespace CriptoFinanceiraDesktop
{
    public partial class RemoverUsuarios : Form
    {
        public static String user;
        public RemoverUsuarios()
        {
            InitializeComponent();
        }

        private void RemoverUsuarios_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            Controle controle = new Controle();
            dgvUsuarios.DataSource = controle.consultarUsuario();
        }

        private void bntExcluir_Click(object sender, EventArgs e)
        {
            if (dgvUsuarios.SelectedRows.Count == 0)
            {
                MessageBox.Show("Nenhum registro selecionado", "Atenção");
            }
            else  
                if (MessageBox.Show("Tem certeza que deseja excluir o usuário selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                user = dgvUsuarios.CurrentRow.Cells[4].Value.ToString();
                dgvUsuarios.Rows.RemoveAt(dgvUsuarios.CurrentRow.Index);
                Controle controle = new Controle();
                controle.removerUsuario(user);
                    MessageBox.Show("Usuário removido com sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Erro ao remover usuário!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
