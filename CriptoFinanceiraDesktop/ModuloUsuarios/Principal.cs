﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CriptoFinanceiraDesktop.Modelo;
using CriptoFinanceiraDesktop.ModuloClientes;
using CriptoFinanceiraDesktop.ModuloSobre;

namespace CriptoFinanceiraDesktop
{
    public partial class Principal : Form
    {

        public bool log = false;

        public Principal()
        {
            InitializeComponent();

        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
                var clientes = new Clientes();
                clientes.MdiParent = this;
                validarFormAberto(clientes);

        }

        private void Principal_Load(object sender, EventArgs e)
        {
            


            if (!log)
            {
                Inicio.recuperarInstancia().criarInstanciaLogin().Show();
                this.Enabled = false;
            }
        
        }

        private void adicionarUsuáriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdicionarUsuario add = new AdicionarUsuario();
            add.MdiParent = this;
            validarFormAberto(add);
            add.WindowState = FormWindowState.Maximized;
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            AdicionarUsuario add = new AdicionarUsuario();
            add.MdiParent = this;
            validarFormAberto(add);
            add.WindowState = FormWindowState.Maximized;
        }

        private void toolStripButton4_Click_1(object sender, EventArgs e)
        {
            RemoverUsuarios del = new RemoverUsuarios();
            del.MdiParent = this;
            validarFormAberto(del);
            del.WindowState = FormWindowState.Maximized;
        }

        private void removerUsuáriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemoverUsuarios del = new RemoverUsuarios();
            del.MdiParent = this;
            validarFormAberto(del);
            //         del.WindowState = FormWindowState.Maximized;
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            ConsultarUsuarios consultar = new ConsultarUsuarios();
            consultar.MdiParent = this;
            validarFormAberto(consultar);
        }

        private void consultarUsuáriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarUsuarios consultar = new ConsultarUsuarios();
            consultar.MdiParent = this;
            validarFormAberto(consultar);
        }

        public void validarFormAberto(Form form)
        {
            if (Application.OpenForms.OfType<Form>().Count() > 1)
            {
                Application.OpenForms.OfType<Form>().First().Focus();
                MessageBox.Show("Feche a janela da atual para abrir uma nova janela","Atenção",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            else
            {
                form.Show();
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            var clientes = new Clientes();
            clientes.MdiParent = this;
            validarFormAberto(clientes);
        }

        private void btnSobre_Click(object sender, EventArgs e)
        {
            var sobre = new Sobre();
            sobre.MdiParent = this;
            validarFormAberto(sobre);
        }

        private void ajudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var sobre = new Sobre();
            sobre.MdiParent = this;
            validarFormAberto(sobre);
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            var invest = new Investimentos();
            invest.MdiParent = this;
            validarFormAberto(invest);
        }

        private void investimentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var invest = new Investimentos();
            invest.MdiParent = this;
            validarFormAberto(invest);
        }

        private void retornoRashesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var rash = new RetornoHashes();
            rash.MdiParent = this;
            validarFormAberto(rash);
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            var rash = new RetornoHashes();
            rash.MdiParent = this;
            validarFormAberto(rash);
        }
    }
}
