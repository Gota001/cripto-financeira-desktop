﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using CriptoFinanceiraDesktop.Backend;
using System.Data;
using System.Windows.Forms;

namespace CriptoFinanceiraDesktop.DAL
{
    class ClientesDao
    {
        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        ConexaoBD con = new ConexaoBD();

        SqlDataReader dr;


        public DataTable consultarTodosClientes()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection conn = new SqlConnection();
                String query = "Select * from tbClientes";
                conn = con.conectar();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                tem = true;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao consultar clientes no banco de Dados!";
            }
            return dt;
        }

        public DataTable consultarTodosInvestimentos()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection conn = new SqlConnection();
                String query = "Select * from tbInvestimentos where status_investimento <> 'aguardando retorno hash'";
                conn = con.conectar();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                tem = true;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao consultar clientes no banco de Dados!";
            }
            return dt;
        }
        public DataTable consultarCliente(String valor_carteira, String nomeCliente)
        {
            DataTable dt = new DataTable();
            try
            {
                String query = "";
               SqlConnection conn = new SqlConnection();

                if (nomeCliente != String.Empty)
                {
                    
                    query = "Select * from tbClientes where valor_carteira " + valor_carteira + 
                    " and nome_cliente = '"+nomeCliente+"'";
                    
                }
                else if (valor_carteira.Equals("todos"))
                {
                    query = "select * from tbClientes";
                }
                else
                {
                    query = "Select * from tbClientes where valor_carteira " + valor_carteira;
                }
                 
                conn = con.conectar();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                tem = true;
                return dt;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao consultar clientes no banco de Dados!";
                return null;
            }
        }

        public DataTable consultarInvestimentos(String moeda, String nome)
        {
            DataTable dt = new DataTable();
            try
            {
                String query = "";
                SqlConnection conn = new SqlConnection();

                if (nome != String.Empty)
                {

                    query = "Select * from tbInvestimentos where status_investimento <> 'aguardando retorno hash' and chave_moeda like '%" + moeda +
                    "%' and chave_cliente like '%" + nome+ "%'";

                }
                else if (moeda.Equals("todas"))
                {
                    query = "select * from tbInvestimentos where status_investimento <> 'aguardando retorno hash'";
                }
                else
                {
                    query = "Select * from tbInvestimentos where status_investimento <> 'aguardando retorno hash' and chave_moeda like '%" + moeda + "%'";
                }

                conn = con.conectar();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                tem = true;
                return dt;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao consultar investimentos no banco de Dados!";
                return null;
            }
        }

        public bool alterarStatusInvestimentos(String user, String alteracao)
        {
            bool tem = false;
            try
            {
                SqlConnection conn = new SqlConnection();
                String query = "UPDATE tbInvestimentos SET status_investimento =" + "'" + alteracao + "' where cod_investimento =" + user;
                SqlCommand cmd = new SqlCommand(query, conn);

                conn = con.conectar();
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                tem = true;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao alterar status investimentos!";
            }
            return tem;
        }

        public DataTable consultarTodosRetornoHash()
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection conn = new SqlConnection();
                String query = "Select * from tbInvestimentos where status_investimento = 'aguardando retorno hash'";
                conn = con.conectar();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                tem = true;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao consultar investimentos no banco de Dados!";
            }
            return dt;
        }
        public DataTable consultarRetornoHash(String moeda, String nome)
        {
            DataTable dt = new DataTable();
            try
            {
                String query = "";
                SqlConnection conn = new SqlConnection();

                if (nome != String.Empty)
                {

                    query = "Select * from tbInvestimentos where status_investimento = 'aguardando retorno hash' and chave_moeda like '%" + moeda +
                    "%' and chave_cliente like '%" + nome + "%'";

                }
                else if (moeda.Equals("todas"))
                {
                    query = "select * from tbInvestimentos where status_investimento = 'aguardando retorno hash'";
                }
                else
                {
                    query = "Select * from tbInvestimentos where status_investimento = 'aguardando retorno hash' and chave_moeda like '%" + moeda + "%'";
                }

                conn = con.conectar();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                tem = true;
                return dt;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao consultar investimentos no banco de Dados!";
                return null;
            }
        }

        public bool descontarValorCarteira(String user, String valor,String id_investimento)
        {
            bool tem = false;

            decimal valor_investimento = Convert.ToDecimal(valor);
            decimal valor_carteira;
            String aux_carteira = "";
            String valor_i;

            try
            {
                SqlConnection conn = new SqlConnection();
                String query = "select valor_carteira from tbClientes where chave_cliente ='" + user +"'";
                SqlCommand cmd = new SqlCommand(query, conn);

                conn = con.conectar();
                cmd.Connection = conn;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read()){ 
                aux_carteira = dr["valor_carteira"].ToString();
                }
                conn.Close();
                valor_carteira = Convert.ToDecimal(aux_carteira);
                valor_carteira -= valor_investimento;
                valor_i = Convert.ToString(valor_carteira);
                valor_i = valor_i.Replace(",",".");

                conn = con.conectar();
                query = "UPDATE tbClientes SET valor_carteira =" + "" + valor_i + " where chave_cliente ='" + user + "'";
                cmd = new SqlCommand(query, conn);
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                conn.Close();

                conn = con.conectar();
                query = "UPDATE tbInvestimentos SET status_investimento ='descontado em carteira' where cod_investimento =" + id_investimento;
                cmd = new SqlCommand(query, conn);
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                tem = true;

            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao descontar valor na carteira virtual do cliente!";
                tem = false;
            }
            return tem;
        }
    }
}
