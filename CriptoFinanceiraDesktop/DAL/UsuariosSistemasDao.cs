﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using CriptoFinanceiraDesktop.Backend;
using System.Data;


namespace CriptoFinanceiraDesktop.DAL
{
    class UsuariosSistemasDao
    {

        public bool tem = false;
        public String mensagem = "";
        SqlCommand cmd = new SqlCommand();
        ConexaoBD con = new ConexaoBD();

        SqlDataReader dr;

        public bool cadastrarUsuário(String nome, String matricula, String cargo, String email, String cpf, String usuario, String senha, String sexo)
        {
            SqlConnection conn = new SqlConnection();
            String query = "insert into tbUsuarios values (@nome,@matricula, @cargo,@email,@cpf,@usuario,@senha,@sexo)";
            SqlCommand cmd = new SqlCommand(query,conn);
            cmd.Parameters.Add(new SqlParameter("@nome", nome));
            cmd.Parameters.Add(new SqlParameter("@matricula", matricula));
            cmd.Parameters.Add(new SqlParameter("@cargo", cargo));
            cmd.Parameters.Add(new SqlParameter("@email", email));
            cmd.Parameters.Add(new SqlParameter("@cpf", cpf));
            cmd.Parameters.Add(new SqlParameter("@usuario", usuario));
            cmd.Parameters.Add(new SqlParameter("@senha", senha));
            cmd.Parameters.Add(new SqlParameter("@sexo", sexo));

            try
            {
                conn = con.conectar();
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                tem = true;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao cadastrar usuário!";
            }
            return tem;
        }

        public DataTable selecionarUsuarios()
        {
            DataTable dt = new DataTable();
            try { 
                SqlConnection conn = new SqlConnection();
                String query = "Select * from tbUsuarios";
                conn = con.conectar();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                tem = true;
            } catch (SqlException e)
            {
                this.mensagem = "Erro ao consultar usuários no banco de Dados!";
            }
            return dt;
        }

        public bool removerUsuarios(String cpf)
        {
            try
            {
                SqlConnection conn = new SqlConnection();
                String query = "delete from tbUsuarios where cpf = @cpf";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.Add(new SqlParameter("@cpf", cpf));
                conn = con.conectar();
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                tem = true;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao excluir usuário!";
            }
            return tem;
        }

        public bool alterar(String campo, String valor, String matricula)
        {
            try
            {
                SqlConnection conn = new SqlConnection();
                String query = "UPDATE tbUsuarios SET "+ campo + "=" +"'" + valor + "' where matricula ='"+ matricula + "'";
                SqlCommand cmd = new SqlCommand(query, conn);
 //               cmd.Parameters.Add(new SqlParameter("@campo", campo));
 //               cmd.Parameters.Add(new SqlParameter("@valor", valor));
                conn = con.conectar();
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                tem = true;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao alterar usuário!";
            }
            return tem;
        }

        public SqlDataReader selecionaUsuario(String matricula)
        {
            SqlDataReader dt;
            try
            {
                SqlConnection conn = new SqlConnection();
                String query = "Select * from tbUsuarios where matricula = @matricula";
                SqlCommand cmd = new SqlCommand(query, conn);
                cmd.Parameters.Add(new SqlParameter("@matricula", matricula));
                conn = con.conectar();

                cmd.Connection = conn;
                dt = cmd.ExecuteReader();
                tem = true;
                return dt;
            }
            catch (SqlException e)
            {
                this.mensagem = "Erro ao consultar usuários no banco de Dados!";
                return null;
            }
        }
    }
}
