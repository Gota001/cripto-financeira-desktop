﻿using CriptoFinanceiraDesktop.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CriptoFinanceiraDesktop.ModuloClientes
{
    public partial class RetornoHashes : Form
    {
        public RetornoHashes()
        {
            InitializeComponent();
        }

        private void RetornoHashes_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            var controle = new ControleClientes();
            dgvHash.DataSource = controle.consultarTodosRetornoHash();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            String moeda = cbMoeda.SelectedItem.ToString();
            if (moeda == String.Empty)
            {
                MessageBox.Show("Selecione uma criptomoeda", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                String nome = txbNome.Text;
                var controle = new ControleClientes();
                if (controle.mensagem != String.Empty)
                {
                    MessageBox.Show(controle.mensagem, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    DataTable dt = controle.consultarRetornoHash(moeda, nome);
                    dgvHash.DataSource = dt;
                }

            }
        }

        private void btnReenviar_Click(object sender, EventArgs e)
        {
            if (dgvHash.SelectedRows.Count == 0)
            {
                MessageBox.Show("Nenhum investimento selecionado", "Atenção");
            }
            else
    if (MessageBox.Show("Tem certeza que deseja reenviar o investimento selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                String user = dgvHash.CurrentRow.Cells[0].Value.ToString();
                dgvHash.Rows.RemoveAt(dgvHash.CurrentRow.Index);
                var controle = new ControleClientes();
                controle.alterarStatusInvestimento(user, "pendente");
                dgvHash.DataSource = controle.consultarTodosRetornoHash();
                MessageBox.Show("investimento reenviado, necessária nova aprovação", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Erro ao reenviar investimento!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDescontar_Click(object sender, EventArgs e)
        {
            if (dgvHash.SelectedRows.Count == 0)
            {
                MessageBox.Show("Nenhum investimento selecionado", "Atenção");
            }
            else
                if (MessageBox.Show("Tem certeza que deseja descontar da carteira do cliente o investimento selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {     
                    String user = dgvHash.CurrentRow.Cells[1].Value.ToString();
                    String valor = dgvHash.CurrentRow.Cells[6].Value.ToString();
                    String id = dgvHash.CurrentRow.Cells[0].Value.ToString();

                    var controle = new ControleClientes();
                    controle.descontarValorCarteira(user,valor,id);
                if (controle.mensagem != String.Empty) {
                    MessageBox.Show(controle.mensagem, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else { 
                    dgvHash.DataSource = controle.consultarTodosRetornoHash();
                    MessageBox.Show("Desconto na carteira virtual efetuado com sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                }
                else
                {
                    MessageBox.Show("Erro ao reenviar investimento!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
        }
    }
}
