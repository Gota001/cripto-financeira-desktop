﻿namespace CriptoFinanceiraDesktop.ModuloClientes
{
    partial class RetornoHashes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RetornoHashes));
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnDescontar = new System.Windows.Forms.Button();
            this.gbClientes = new System.Windows.Forms.GroupBox();
            this.lblNome = new System.Windows.Forms.Label();
            this.txbNome = new System.Windows.Forms.TextBox();
            this.lblCriptomoedas = new System.Windows.Forms.Label();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.cbMoeda = new System.Windows.Forms.ComboBox();
            this.dgvHash = new System.Windows.Forms.DataGridView();
            this.btnReenviar = new System.Windows.Forms.Button();
            this.gbClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHash)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(1176, 575);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(155, 23);
            this.btnCancelar.TabIndex = 10;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnDescontar
            // 
            this.btnDescontar.Location = new System.Drawing.Point(1015, 575);
            this.btnDescontar.Name = "btnDescontar";
            this.btnDescontar.Size = new System.Drawing.Size(155, 23);
            this.btnDescontar.TabIndex = 9;
            this.btnDescontar.Text = "Descontar carteira";
            this.btnDescontar.UseVisualStyleBackColor = true;
            this.btnDescontar.Click += new System.EventHandler(this.btnDescontar_Click);
            // 
            // gbClientes
            // 
            this.gbClientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.gbClientes.Controls.Add(this.lblNome);
            this.gbClientes.Controls.Add(this.txbNome);
            this.gbClientes.Controls.Add(this.lblCriptomoedas);
            this.gbClientes.Controls.Add(this.btnConsultar);
            this.gbClientes.Controls.Add(this.cbMoeda);
            this.gbClientes.Location = new System.Drawing.Point(5, 12);
            this.gbClientes.Name = "gbClientes";
            this.gbClientes.Size = new System.Drawing.Size(1336, 100);
            this.gbClientes.TabIndex = 8;
            this.gbClientes.TabStop = false;
            this.gbClientes.Text = "Parâmetros de consulta retorno de rash ";
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(299, 27);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(86, 13);
            this.lblNome.TabIndex = 6;
            this.lblNome.Text = "Nome (Opcional)";
            // 
            // txbNome
            // 
            this.txbNome.Location = new System.Drawing.Point(302, 46);
            this.txbNome.Name = "txbNome";
            this.txbNome.Size = new System.Drawing.Size(321, 20);
            this.txbNome.TabIndex = 5;
            // 
            // lblCriptomoedas
            // 
            this.lblCriptomoedas.AutoSize = true;
            this.lblCriptomoedas.Location = new System.Drawing.Point(5, 27);
            this.lblCriptomoedas.Name = "lblCriptomoedas";
            this.lblCriptomoedas.Size = new System.Drawing.Size(66, 13);
            this.lblCriptomoedas.TabIndex = 4;
            this.lblCriptomoedas.Text = "Criptomoeda";
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(658, 43);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(155, 23);
            this.btnConsultar.TabIndex = 2;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // cbMoeda
            // 
            this.cbMoeda.FormattingEnabled = true;
            this.cbMoeda.Items.AddRange(new object[] {
            "Aeon (AEON)",
            "Auroracoin (AUR)",
            "Bitcoin Cash (BCH)",
            "Bitcoin (BTC)",
            "BlackCoin (BC, BLK)",
            "CloakCoin (CLOAK[38])",
            "Dash (DASH)",
            "DigitalNote (XDN)",
            "Dogecoin (DOGE)",
            "E-Coins (ECS)",
            "Emercoin (EMC)",
            "EOS.IO (EOS)",
            "Ethereum Classic (ETC)",
            "Ethereum (ETH)",
            "Gridcoin (GRC)",
            "Litecoin (LTC)",
            "Lunes (LUNES)",
            "Mastercoin (MSC)",
            "MazaCoin(MZC)",
            "Monero (XMR)",
            "Namecoin (NMC)",
            "Nano[37] (NANO)",
            "NEM (XEM)",
            "NEO (NEO)",
            "Niobio Cash (NBR)",
            "Niobium Coin (NBC)",
            "Nxt (NXT)",
            "Peercoin (PPC)",
            "PotCoin (POT)",
            "Primecoin (XPM)",
            "Ripple (XRP)",
            "Stellar (XLM)",
            "Tether (USDT)",
            "Titcoin (TIT)",
            "Verge (XVG)",
            "Vertcoin (VTC)",
            "Zcash (ZEC)",
            "todas"});
            this.cbMoeda.Location = new System.Drawing.Point(8, 46);
            this.cbMoeda.Name = "cbMoeda";
            this.cbMoeda.Size = new System.Drawing.Size(273, 21);
            this.cbMoeda.TabIndex = 1;
            // 
            // dgvHash
            // 
            this.dgvHash.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvHash.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHash.Location = new System.Drawing.Point(5, 121);
            this.dgvHash.Name = "dgvHash";
            this.dgvHash.Size = new System.Drawing.Size(1336, 422);
            this.dgvHash.TabIndex = 7;
            // 
            // btnReenviar
            // 
            this.btnReenviar.Location = new System.Drawing.Point(854, 575);
            this.btnReenviar.Name = "btnReenviar";
            this.btnReenviar.Size = new System.Drawing.Size(155, 23);
            this.btnReenviar.TabIndex = 11;
            this.btnReenviar.Text = "Reenviar";
            this.btnReenviar.UseVisualStyleBackColor = true;
            this.btnReenviar.Click += new System.EventHandler(this.btnReenviar_Click);
            // 
            // RetornoHashes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1344, 620);
            this.Controls.Add(this.btnReenviar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnDescontar);
            this.Controls.Add(this.gbClientes);
            this.Controls.Add(this.dgvHash);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RetornoHashes";
            this.Text = "RetornoHashes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.RetornoHashes_Load);
            this.gbClientes.ResumeLayout(false);
            this.gbClientes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHash)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnDescontar;
        private System.Windows.Forms.GroupBox gbClientes;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.TextBox txbNome;
        private System.Windows.Forms.Label lblCriptomoedas;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.ComboBox cbMoeda;
        private System.Windows.Forms.DataGridView dgvHash;
        private System.Windows.Forms.Button btnReenviar;
    }
}