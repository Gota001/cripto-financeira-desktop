﻿using CriptoFinanceiraDesktop.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CriptoFinanceiraDesktop.ModuloClientes
{
    public partial class Investimentos : Form
    {
        public Investimentos()
        {
            InitializeComponent();
        }

        private void Investimentos_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            var controle = new ControleClientes();
            dgvInvestimentos.DataSource = controle.consultarTodosInvestimentos();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            String moeda = cbCriptomoeda.SelectedItem.ToString();
            if (moeda == String.Empty)
            {
                MessageBox.Show("Selecione uma criptomoeda" ,"Aviso",MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                String nome = txbNome.Text;
                var controle = new ControleClientes();
                if (controle.mensagem != String.Empty)
                {
                    MessageBox.Show(controle.mensagem, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    DataTable dt = controle.consultarInvestimentos(moeda,nome);
                    dgvInvestimentos.DataSource = dt;
                }
                
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAprovar_Click(object sender, EventArgs e)
        {
            if (dgvInvestimentos.SelectedRows.Count == 0)
            {
                MessageBox.Show("Nenhum investimento selecionado", "Atenção");
            }
            else
                if (MessageBox.Show("Tem certeza que deseja aprovar o investimento selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                String user = dgvInvestimentos.CurrentRow.Cells[0].Value.ToString();
                dgvInvestimentos.Rows.RemoveAt(dgvInvestimentos.CurrentRow.Index);
                var controle = new ControleClientes();
                controle.alterarStatusInvestimento(user,"aguardando retorno hash");
                dgvInvestimentos.DataSource = controle.consultarTodosInvestimentos();
                MessageBox.Show("investimento aprovado com sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Erro ao aprovar investimento!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnReprovar_Click(object sender, EventArgs e)
        {
            if (dgvInvestimentos.SelectedRows.Count == 0)
            {
                MessageBox.Show("Nenhum investimento selecionado", "Atenção");
            }
            else
                if (MessageBox.Show("Tem certeza que deseja reprovar o investimento selecionado?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                String user = dgvInvestimentos.CurrentRow.Cells[0].Value.ToString();
                dgvInvestimentos.Rows.RemoveAt(dgvInvestimentos.CurrentRow.Index);
                var controle = new ControleClientes();
                controle.alterarStatusInvestimento(user,"reprovado");
                dgvInvestimentos.DataSource = controle.consultarTodosInvestimentos();
                MessageBox.Show("investimento reprovado com sucesso!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Erro ao reprovar investimento!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
