﻿namespace CriptoFinanceiraDesktop.ModuloClientes
{
    partial class Investimentos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Investimentos));
            this.gbClientes = new System.Windows.Forms.GroupBox();
            this.lblNome = new System.Windows.Forms.Label();
            this.txbNome = new System.Windows.Forms.TextBox();
            this.lblCriptomoedas = new System.Windows.Forms.Label();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.cbCriptomoeda = new System.Windows.Forms.ComboBox();
            this.dgvInvestimentos = new System.Windows.Forms.DataGridView();
            this.btnAprovar = new System.Windows.Forms.Button();
            this.btnReprovar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.gbClientes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvestimentos)).BeginInit();
            this.SuspendLayout();
            // 
            // gbClientes
            // 
            this.gbClientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.gbClientes.Controls.Add(this.lblNome);
            this.gbClientes.Controls.Add(this.txbNome);
            this.gbClientes.Controls.Add(this.lblCriptomoedas);
            this.gbClientes.Controls.Add(this.btnConsultar);
            this.gbClientes.Controls.Add(this.cbCriptomoeda);
            this.gbClientes.Location = new System.Drawing.Point(4, 9);
            this.gbClientes.Name = "gbClientes";
            this.gbClientes.Size = new System.Drawing.Size(1337, 100);
            this.gbClientes.TabIndex = 3;
            this.gbClientes.TabStop = false;
            this.gbClientes.Text = "Parâmetros de consulta de investimentos";
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(299, 27);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(86, 13);
            this.lblNome.TabIndex = 6;
            this.lblNome.Text = "Nome (Opcional)";
            // 
            // txbNome
            // 
            this.txbNome.Location = new System.Drawing.Point(302, 46);
            this.txbNome.Name = "txbNome";
            this.txbNome.Size = new System.Drawing.Size(321, 20);
            this.txbNome.TabIndex = 5;
            // 
            // lblCriptomoedas
            // 
            this.lblCriptomoedas.AutoSize = true;
            this.lblCriptomoedas.Location = new System.Drawing.Point(5, 27);
            this.lblCriptomoedas.Name = "lblCriptomoedas";
            this.lblCriptomoedas.Size = new System.Drawing.Size(66, 13);
            this.lblCriptomoedas.TabIndex = 4;
            this.lblCriptomoedas.Text = "Criptomoeda";
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(658, 43);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(155, 23);
            this.btnConsultar.TabIndex = 2;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // cbCriptomoeda
            // 
            this.cbCriptomoeda.FormattingEnabled = true;
            this.cbCriptomoeda.Items.AddRange(new object[] {
            "Aeon (AEON)",
            "Auroracoin (AUR)",
            "Bitcoin Cash (BCH)",
            "Bitcoin (BTC)",
            "BlackCoin (BC, BLK)",
            "CloakCoin (CLOAK[38])",
            "Dash (DASH)",
            "DigitalNote (XDN)",
            "Dogecoin (DOGE)",
            "E-Coins (ECS)",
            "Emercoin (EMC)",
            "EOS.IO (EOS)",
            "Ethereum Classic (ETC)",
            "Ethereum (ETH)",
            "Gridcoin (GRC)",
            "Litecoin (LTC)",
            "Lunes (LUNES)",
            "Mastercoin (MSC)",
            "MazaCoin(MZC)",
            "Monero (XMR)",
            "Namecoin (NMC)",
            "Nano[37] (NANO)",
            "NEM (XEM)",
            "NEO (NEO)",
            "Niobio Cash (NBR)",
            "Niobium Coin (NBC)",
            "Nxt (NXT)",
            "Peercoin (PPC)",
            "PotCoin (POT)",
            "Primecoin (XPM)",
            "Ripple (XRP)",
            "Stellar (XLM)",
            "Tether (USDT)",
            "Titcoin (TIT)",
            "Verge (XVG)",
            "Vertcoin (VTC)",
            "Zcash (ZEC)",
            "todas"});
            this.cbCriptomoeda.Location = new System.Drawing.Point(8, 46);
            this.cbCriptomoeda.Name = "cbCriptomoeda";
            this.cbCriptomoeda.Size = new System.Drawing.Size(273, 21);
            this.cbCriptomoeda.TabIndex = 1;
            // 
            // dgvInvestimentos
            // 
            this.dgvInvestimentos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvInvestimentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInvestimentos.Location = new System.Drawing.Point(4, 118);
            this.dgvInvestimentos.Name = "dgvInvestimentos";
            this.dgvInvestimentos.Size = new System.Drawing.Size(1337, 422);
            this.dgvInvestimentos.TabIndex = 2;
            // 
            // btnAprovar
            // 
            this.btnAprovar.Location = new System.Drawing.Point(854, 561);
            this.btnAprovar.Name = "btnAprovar";
            this.btnAprovar.Size = new System.Drawing.Size(155, 23);
            this.btnAprovar.TabIndex = 4;
            this.btnAprovar.Text = "Aprovar";
            this.btnAprovar.UseVisualStyleBackColor = true;
            this.btnAprovar.Click += new System.EventHandler(this.btnAprovar_Click);
            // 
            // btnReprovar
            // 
            this.btnReprovar.Location = new System.Drawing.Point(1015, 561);
            this.btnReprovar.Name = "btnReprovar";
            this.btnReprovar.Size = new System.Drawing.Size(155, 23);
            this.btnReprovar.TabIndex = 5;
            this.btnReprovar.Text = "Reprovar";
            this.btnReprovar.UseVisualStyleBackColor = true;
            this.btnReprovar.Click += new System.EventHandler(this.btnReprovar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(1176, 561);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(155, 23);
            this.btnCancelar.TabIndex = 6;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // Investimentos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1344, 620);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnReprovar);
            this.Controls.Add(this.btnAprovar);
            this.Controls.Add(this.gbClientes);
            this.Controls.Add(this.dgvInvestimentos);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Investimentos";
            this.Text = "Investimentos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Investimentos_Load);
            this.gbClientes.ResumeLayout(false);
            this.gbClientes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvestimentos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbClientes;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.TextBox txbNome;
        private System.Windows.Forms.Label lblCriptomoedas;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.ComboBox cbCriptomoeda;
        private System.Windows.Forms.DataGridView dgvInvestimentos;
        private System.Windows.Forms.Button btnAprovar;
        private System.Windows.Forms.Button btnReprovar;
        private System.Windows.Forms.Button btnCancelar;
    }
}