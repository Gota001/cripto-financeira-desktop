﻿using CriptoFinanceiraDesktop.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CriptoFinanceiraDesktop.Modelo;

namespace CriptoFinanceiraDesktop
{
    public partial class Clientes : Form
    {
        public Clientes()
        {
            InitializeComponent();
        }

        private void Clientes_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            var controle = new ControleClientes();
            dgvClientes.DataSource = controle.consultarTodosClientes();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {

            bool cbvazio = false;



            if(cbClientes.SelectedItem == null)
            {
               cbvazio = true;
            }

            if (cbvazio)
            {
                MessageBox.Show("Selecione um tipo de cliente","Atenção",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
            else 
            { 
                 String tipoCliente = cbClientes.SelectedItem.ToString();
                 String nomeCliente = "";

                if (tipoCliente.Equals("Todos"))
                {
                    txbNome.Text = "";
                }
                if (txbNome.Text != String.Empty)
                {
                    nomeCliente = txbNome.Text;
                }
                ControleClientes controle = new ControleClientes();
                if (controle.mensagem != String.Empty)
                {
                    MessageBox.Show(controle.mensagem, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    DataTable dt = controle.consultarCliente(tipoCliente, nomeCliente);
                    dgvClientes.DataSource = dt;
                }

            }
        }
    }
}
