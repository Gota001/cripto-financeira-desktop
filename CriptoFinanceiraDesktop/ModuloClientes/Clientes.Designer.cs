﻿namespace CriptoFinanceiraDesktop
{
    partial class Clientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Clientes));
            this.dgvClientes = new System.Windows.Forms.DataGridView();
            this.gbClientes = new System.Windows.Forms.GroupBox();
            this.lblNome = new System.Windows.Forms.Label();
            this.txbNome = new System.Windows.Forms.TextBox();
            this.lblTipoClientes = new System.Windows.Forms.Label();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.cbClientes = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).BeginInit();
            this.gbClientes.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvClientes
            // 
            this.dgvClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClientes.Location = new System.Drawing.Point(4, 122);
            this.dgvClientes.Name = "dgvClientes";
            this.dgvClientes.Size = new System.Drawing.Size(1337, 494);
            this.dgvClientes.TabIndex = 0;
            // 
            // gbClientes
            // 
            this.gbClientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.gbClientes.Controls.Add(this.lblNome);
            this.gbClientes.Controls.Add(this.txbNome);
            this.gbClientes.Controls.Add(this.lblTipoClientes);
            this.gbClientes.Controls.Add(this.btnConsultar);
            this.gbClientes.Controls.Add(this.cbClientes);
            this.gbClientes.Location = new System.Drawing.Point(4, 13);
            this.gbClientes.Name = "gbClientes";
            this.gbClientes.Size = new System.Drawing.Size(1337, 100);
            this.gbClientes.TabIndex = 1;
            this.gbClientes.TabStop = false;
            this.gbClientes.Text = "Parâmetros de consulta de clientes";
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Location = new System.Drawing.Point(299, 27);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(86, 13);
            this.lblNome.TabIndex = 6;
            this.lblNome.Text = "Nome (Opcional)";
            // 
            // txbNome
            // 
            this.txbNome.Location = new System.Drawing.Point(302, 46);
            this.txbNome.Name = "txbNome";
            this.txbNome.Size = new System.Drawing.Size(321, 20);
            this.txbNome.TabIndex = 5;
            // 
            // lblTipoClientes
            // 
            this.lblTipoClientes.AutoSize = true;
            this.lblTipoClientes.Location = new System.Drawing.Point(9, 27);
            this.lblTipoClientes.Name = "lblTipoClientes";
            this.lblTipoClientes.Size = new System.Drawing.Size(68, 13);
            this.lblTipoClientes.TabIndex = 3;
            this.lblTipoClientes.Text = "Tipo Clientes";
            // 
            // btnConsultar
            // 
            this.btnConsultar.Location = new System.Drawing.Point(643, 43);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(155, 23);
            this.btnConsultar.TabIndex = 2;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // cbClientes
            // 
            this.cbClientes.FormattingEnabled = true;
            this.cbClientes.Items.AddRange(new object[] {
            "Com Investimentos Ativos",
            "Sem Investimento Ativo",
            "Todos"});
            this.cbClientes.Location = new System.Drawing.Point(8, 46);
            this.cbClientes.Name = "cbClientes";
            this.cbClientes.Size = new System.Drawing.Size(274, 21);
            this.cbClientes.TabIndex = 0;
            // 
            // Clientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1344, 620);
            this.Controls.Add(this.gbClientes);
            this.Controls.Add(this.dgvClientes);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Clientes";
            this.Text = "Clientes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Clientes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvClientes)).EndInit();
            this.gbClientes.ResumeLayout(false);
            this.gbClientes.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvClientes;
        private System.Windows.Forms.GroupBox gbClientes;
        private System.Windows.Forms.Label lblTipoClientes;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.ComboBox cbClientes;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.TextBox txbNome;
    }
}