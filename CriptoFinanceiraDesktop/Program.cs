﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using CriptoFinanceiraDesktop.Modelo;

namespace CriptoFinanceiraDesktop
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(Inicio.recuperarInstancia().criarInstanciaPrincipal());
 //         RemoverUsuarios c = new RemoverUsuarios();
 //         Application.Run(c);
        }
    }
}
