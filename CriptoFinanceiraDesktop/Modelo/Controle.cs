﻿using CriptoFinanceiraDesktop.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using CriptoFinanceiraDesktop.Backend;

namespace CriptoFinanceiraDesktop.Modelo
{
    class Controle
    {
        public bool tem;
        public String usuario;
        public String senha;
        public String mensagem = "";

        public bool acessar(String login, String senha)
        {
            LoginDao loginDao = new LoginDao();
            tem = loginDao.verificarLogin(login, senha);
            if (!loginDao.mensagem.Equals(""))
            {
                this.mensagem = loginDao.mensagem;
            }
            return tem;
        }
        public bool cadastrar(String nome, String matricula, String cargo, String email, String cpf, String sexo)
        {
            String aux = nome.Replace(" ", "");
            Random random = new Random(aux.Length);
            Random random2 = new Random(9);
            senha = "";
            string caracteresPermitidos = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";
            char [] usuario2 = aux.ToCharArray();

            for (int i = 0; i < 6; i++) {
                int r = random.Next(9);
                usuario = usuario + usuario2[r].ToString();
                senha = senha + caracteresPermitidos[random2.Next(r, caracteresPermitidos.Length)];
            }

            usuario = usuario.Substring(0, 6);
            senha = senha.Substring(0,6);

           UsuariosSistemasDao sts = new UsuariosSistemasDao();
           tem =  sts.cadastrarUsuário(nome, matricula, cargo, email, cpf, usuario, senha, sexo);
            if (!sts.mensagem.Equals(""))
            {
                this.mensagem = sts.mensagem;
            }

            return tem;
        }

        public DataTable consultarUsuario()
        {   
            UsuariosSistemasDao sts = new UsuariosSistemasDao();
            DataTable dt = sts.selecionarUsuarios();
            if (!sts.mensagem.Equals(""))
            {
                this.mensagem = sts.mensagem;
            }

            return dt;
        }

        public bool removerUsuario(String cpf)
        {
            UsuariosSistemasDao sts = new UsuariosSistemasDao();
            tem = sts.removerUsuarios(cpf);
            if (!sts.mensagem.Equals(""))
            {
                this.mensagem = sts.mensagem;
            }

            return tem;
        }

        public SqlDataReader selecionarUsuario(String matricula)
        {
            UsuariosSistemasDao sts = new UsuariosSistemasDao();
            SqlDataReader dr = sts.selecionaUsuario(matricula);
            if (!sts.mensagem.Equals(""))
            {
                this.mensagem = sts.mensagem;
            }
            return dr;
        }

        public bool alterar(String campo, String valor, String matricula)
        {
            UsuariosSistemasDao sts = new UsuariosSistemasDao();
            tem = sts.alterar(campo,valor,matricula);
            if (!sts.mensagem.Equals(""))
            {
                this.mensagem = sts.mensagem;
            }

            return tem;
        }

        public String[] consulta(String matricula)
        {
            String query = "Select * from tbUsuarios where matricula=@matricula";
            ConexaoBD conn = new ConexaoBD();
            SqlConnection con = conn.conectar();
            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.Add(new SqlParameter("@matricula", matricula));
            cmd.Connection = con;
            SqlDataReader dr = cmd.ExecuteReader();
            String[] texto = {"","","","",""};

            if (dr.Read())
            {
                texto[0] = dr["nome"].ToString();
                texto[1] = dr["cargo"].ToString();
                texto[2] = dr["email"].ToString();
                texto[3] = dr["cpf"].ToString();
                texto[4] = dr["sexo"].ToString();
            }
            return texto;
        }
    }

}
 

