﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using CriptoFinanceiraDesktop.DAL;
using System.Data;
using System.CodeDom.Compiler;

namespace CriptoFinanceiraDesktop.Modelo
{
    class ControleClientes
    {

        public bool tem;
        public String mensagem = "";

        public DataTable consultarTodosClientes()
        {
            ClientesDao sts = new ClientesDao();
            DataTable dt = sts.consultarTodosClientes();
            if (!sts.mensagem.Equals(""))
            {
                this.mensagem = sts.mensagem;
            }

            return dt;
        }

        public DataTable consultarCliente(String tipoCliente, String nomeCliente)
        {
            ClientesDao sts = new ClientesDao();
            String valor_carteira = "";

            switch (tipoCliente)
            {
                case "Com Investimentos Ativos":
                    valor_carteira = "<>0";
                    break;
                case "Sem Investimento Ativo":
                    valor_carteira = "= 0";
                    break;
                case "Todos":
                    valor_carteira = "todos";
                    break;
            }

            DataTable dt = sts.consultarCliente(valor_carteira, nomeCliente);
            if (!sts.mensagem.Equals(""))
            {
                this.mensagem = sts.mensagem;
            }

            return dt;
        }
        public DataTable consultarInvestimentos(String moeda, String nome)
        {
            ClientesDao sts = new ClientesDao();

            DataTable dt = sts.consultarInvestimentos(moeda, nome);

            if (!sts.mensagem.Equals(""))
            {
                this.mensagem = sts.mensagem;
            }
            return dt;
        }

        public DataTable consultarRetornoHash(String moeda, String nome)
        {
            ClientesDao sts = new ClientesDao();

            DataTable dt = sts.consultarRetornoHash(moeda, nome);

            if (!sts.mensagem.Equals(""))
            {
                this.mensagem = sts.mensagem;
            }
            return dt;
        }
        public DataTable consultarTodosInvestimentos()
        {
            ClientesDao sts = new ClientesDao();

            DataTable dt = sts.consultarTodosInvestimentos();

            if (!sts.mensagem.Equals(""))
            {
                this.mensagem = sts.mensagem;
            }
            return dt;
        }

        public DataTable consultarTodosRetornoHash()
        {
            ClientesDao sts = new ClientesDao();

            DataTable dt = sts.consultarTodosRetornoHash();

            if (!sts.mensagem.Equals(""))
            {
                this.mensagem = sts.mensagem;
            }
            return dt;
        }

        public bool alterarStatusInvestimento(String user, String alteracao)
        {
            ClientesDao sts = new ClientesDao();
            tem = sts.alterarStatusInvestimentos(user, alteracao);
            if (!sts.mensagem.Equals(""))
            {
                this.mensagem = sts.mensagem;
            }

            return tem;
        }

        public bool descontarValorCarteira(String user, String valor, String id)
        {
            ClientesDao sts = new ClientesDao();
            tem = sts.descontarValorCarteira(user, valor,id);
            if (!sts.mensagem.Equals(""))
            {
                this.mensagem = sts.mensagem;
            }

            return tem;
        }
    }
}
