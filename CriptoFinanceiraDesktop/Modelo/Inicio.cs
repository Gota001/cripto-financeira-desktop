﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CriptoFinanceiraDesktop.Modelo
{
    class Inicio
    {

        private static Inicio instance;
        
        public static Login instanceLogin;
        public Principal instancePrincipal;


        public void iniciar()
        {
            criarInstanciaLogin();
            criarInstanciaPrincipal();
        }
        public Login criarInstanciaLogin()
        {
            if(instanceLogin == null)
            {
                instanceLogin = new Login();
            }
            return instanceLogin;
        }

        public Principal criarInstanciaPrincipal()
        {
            if (instancePrincipal == null)
            {
                instancePrincipal = new Principal();
            }
            return instancePrincipal;
        }

        public static Inicio recuperarInstancia()
        {
            if (instance == null)
            {
                instance = new Inicio();
            }
            return instance;
        }


    }
}
